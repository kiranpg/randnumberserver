package main

import (
	"fmt"
	"math/rand"
	"net/http"
	"time"
)

func callRecipientHandler(w http.ResponseWriter, r *http.Request) {
	numbers := []string {
		"09886734047",
		"09003010649",
	}
	rand.Seed(time.Now().Unix())
	option := rand.Int() % len(numbers)

	fmt.Fprintf(w, numbers[option])
}

func main(){
	http.HandleFunc("/recipient", callRecipientHandler)
	http.ListenAndServe(":8080", nil)
}
